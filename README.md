## Thanks for your consideration. 

I have split all the tasks into relevant folders with a readme explaining my answer or solution.
Where relevant I have provided examples or modifications in index.js.

Some implementations are bare bones to demonstrate a point. In a production system they may operate very differently.
I have tried to answer the questions in the spirit I believe they were written. 

Thank you for your time, If you have any questions or comments please dont hesitate to pass them on to me!

Morgan Owen.
