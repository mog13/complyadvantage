This code will return `1` as is and `10` when you remove `function a() {}`.
I am not immediately sure why this is but stepping through the debugger I am assuming the presence of the the function will
redefine the scope of a within `b()`. With it then `a` is internal and does not effect the global variable. Without it the internal `a=10` is operating on the global `a` and so the output is changed.