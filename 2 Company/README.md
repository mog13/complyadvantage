## company task

Based on the wording of the question I have stayed away from es6 classes (or the use of TS) which wrap a lot of this functionality up.
I also realise in this example we dont use any internal functions so its not strictly necessary to copy the prototype across ( it works without it).
However this would be important when fleshing the classes out to make sure inherited classes fall back to there supers.