function Employee(name) {
    this.name = name || "";
    this.department = "general"

}

function Manager(name, reports) {
    Employee.call(this,name);
    this.reports = [].concat(reports); //force it to be an array;
}
Manager.prototype = Object.create(Employee.prototype);

function WorkBee(name) {
    Employee.call(this,name);
    this.projectName = ""
}
WorkBee.prototype = Object.create(Employee.prototype);

function SalesPerson(name, revenue) {
    WorkBee.call(this,name);
    this.department = "sales";
    this.projectName = "internal";
    this.revenue = revenue ||0;
}
SalesPerson.prototype = Object.create(WorkBee.prototype);

function SoftwareEngineer(name, techSkills, projectName) {
    WorkBee.call(this,name);
    this.department = "tech";
    this.projectName = projectName ||"App-ComplyAdvantage" ;
    this.techSkills = [].concat(techSkills);
}
SoftwareEngineer.prototype = Object.create(WorkBee.prototype);

let John = new Manager("John Doe", [{name: "Q1", "statistics": ""}, {name: "Q2", statistics: "..."}]);

console.log(John.department); // General
console.log(John.name);// John Doe
console.log(John.reports); // [{name: "Q1", "statistics": ...}, {name: "Q2", statistics: ...}]

let Michael = new SalesPerson("Michael T", 2540);

console.log(Michael.department); // sales
console.log(Michael.name); // Michael T
console.log(Michael.projectName); // internal
console.log(Michael.revenue); // 2540

let Joseph = new SoftwareEngineer("Joseph K. Ellis", ["Javascript", "HTML"], "App-ComplyAdvantage");

console.log(Joseph.department); // tech
console.log(Joseph.name); // Joseph K. Ellis
console.log(Joseph.projectName); // App-ComplyAdvantage
console.log(Joseph.techSkills); // ["Javascript", "HTML"]
