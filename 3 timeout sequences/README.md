## Timeout sequences

This prints out `10` 10 times. This is because `setTimeout` (even when called with 0) is put on the message queue and not the call stack. 
The for loop opperates on the callstack and so will be prioritized and the timeout callback only executed in the next tick (when i will be 10).

this can be advantageous however if you needed 1-10 there are different ways to achieve this depending on the situation.
You could not use the timeout at all or you can bring the variable within the scope. both are demonstrated in index.js 