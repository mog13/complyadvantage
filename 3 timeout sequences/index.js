console.log("no timeout");
for(let i = 0; i < 10; i++) {
        console.log(i);
}

console.log("use a scoped variable");
for(let i = 0; i < 10; i++) {
    let n = i;
    setTimeout(function() {
        console.log(n);
    }, 0);
}
