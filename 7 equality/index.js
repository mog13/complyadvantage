if(!Object.isEqual) {
    Object.prototype.isEqual = function (comparisonObj)
    {


        let equal = true;
        //check the comparison object exists, its an object and has the same amount of keys (we know this is because this function exists on Object
        if(!comparisonObj || comparisonObj instanceof Object && Object.keys(comparisonObj).length !== Object.keys(this).length) return false;
        for (let key of Object.keys(comparisonObj)){
            //check the property exists on self
            if (this[key]) {
                //hasOwnProperty could be checked if we want to also check inherited properties
                let comparisonValue = comparisonObj[key];
                let value = this[key];
                //if there both object check the object are the same (deep comparison using recursion)
                if (comparisonValue instanceof Object && value instanceof Object) {
                    if (!value.isEqual(comparisonValue)) {
                        equal = false;
                    }
                } else if (value !== comparisonValue) {
                    equal = false;
                }
            } else {
                equal = false;

            }
             if (!equal) break // no point checking more if ones not equal
        }
        return equal;
    };
}


let obj1 = {
    name: "John",
    age: 10,
    info: {
        address: 'some place',
        tel: '0123456789',
        hobbies: ['1', '2']
    }
};

let obj2 = {
    name: "John",
    age: 10,
    info: {
        address: 'some place',
        tel: '0123456789',
        hobbies: ['1', '2']
    }
};

let smallObj = {
    name: "John",
    age: 10
};


let obj3 = obj2;
let obj4 = obj1;

console.log(smallObj.isEqual(obj1)); //return false;
console.log(smallObj.isEqual({name: "John"})); //return false;
console.log(obj1.isEqual(undefined)); //return false;
console.log(obj2.isEqual(obj1)); //returns true;
console.log(obj3.isEqual(obj1)); //returns true;
console.log(obj4.isEqual(obj1)); //returns true;
