map returns a new array. even though things are passed by value in JS an objects value is its reference.
that means that despite making a new array we are filling it with references to the same objects and so
changes in either array mutate the original.
To get around this we need to make sure we create a new copy of each object. I achieved this by using the spread operate to populate an object.
This could also be done with Object assign or iterating over it.
This method would not work for nested objects for the same reason and would require modification (like`isEqual` prototype exercise to work recursively)