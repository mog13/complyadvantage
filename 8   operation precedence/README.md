the original script will output 5. This is because operation precedence in JS is right to left.
This results in b becoming global in scope (hence the output) but a remaining local to the IIFE.
Providing you want the variables to be private then you need to declare them first and then you can use a similar syntax as suggested.
An example of this in in `index.js`
