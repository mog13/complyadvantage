this will print: `script start`,`script end`,`promise1`,`promise2`,`setTimeout`.

This is because `script start` is hit and immediately executed.`set timout` is then added to the message queue.
A promise is then made and added to the event loop ontop of the `set timeout`.
`script end` is then hit and printed. Once this is done the callstack is empty so the message queue will be read.
this is read as last in first out so then promise is then fulfilled. `promise1` is printed and the `then` functioned queued and executed printing `promise2`.
finally the timeout callback is run resulting in `set timeout`.

(n.b im aware there is more to the event loop and several different queues but I believe the most important distinction is in the priority of the callstack over the queue) 