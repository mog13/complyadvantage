function* Fibonacci() {
    let current =0, previous = 0;
 while(true) {
     let temp = previous; //store it do we can change
     previous = current;
    current = previous + temp  ||1;
     let reset = yield current; // stop here and wait for next input store for reset
     if(reset) {
         current = 0;
         previous = 0;
     }
 }
}


// let fibGen = Fibonacci();
//
// console.log(fibGen.next().value);
// console.log(fibGen.next().value);
// console.log(fibGen.next().value);
// console.log(fibGen.next().value);
// console.log(fibGen.next().value);
// console.log(fibGen.next(true).value);
// console.log(fibGen.next().value);
// console.log(fibGen.next().value);
