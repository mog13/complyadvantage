## Fibonacci generator

I chose to implement this using the new es6 feature generators as it seems one of the ideal use cases and keeps the scope private.
Its a slight change to the function signature suggested which I guessed was fine as the question called to create.
If it had to remain the same I would use an internal closure (/function) to keep the variables out of scope. 
This can be seen ub the webpage implementation where i use an IIFE to hide the variable `fibGen`

In a production environment I would use Webpack to create a library bundle that can be easily consumed with UMD (AMD/common) or via a script tag.
Since it only needs to be consumed in a webpage I have just left the generator exposed for consumption in  a script tag. 

I have used some functionality that may not work on all browsers as in a full application the build pipeline would account for this with Babel etc. and im keen to use the cleanest looking code given the circumstances.